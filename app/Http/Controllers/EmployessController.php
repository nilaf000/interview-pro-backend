<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateEmployessRequest;
use App\Http\Requests\UpdateEmployessRequest;
use App\Repositories\EmployessRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class EmployessController extends AppBaseController
{
    /** @var  EmployessRepository */
    private $employessRepository;

    public function __construct(EmployessRepository $employessRepo)
    {
        $this->employessRepository = $employessRepo;
    }

    /**
     * Display a listing of the Employess.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $employesses = $this->employessRepository->all();

        return view('employesses.index')
            ->with('employesses', $employesses);
    }

    /**
     * Show the form for creating a new Employess.
     *
     * @return Response
     */
    public function create()
    {
        return view('employesses.create');
    }

    /**
     * Store a newly created Employess in storage.
     *
     * @param CreateEmployessRequest $request
     *
     * @return Response
     */
    public function store(CreateEmployessRequest $request)
    {
        $input = $request->all();

        $employess = $this->employessRepository->create($input);

        Flash::success('Employess saved successfully.');

        return redirect(route('employesses.index'));
    }

    /**
     * Display the specified Employess.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $employess = $this->employessRepository->find($id);

        if (empty($employess)) {
            Flash::error('Employess not found');

            return redirect(route('employesses.index'));
        }

        return view('employesses.show')->with('employess', $employess);
    }

    /**
     * Show the form for editing the specified Employess.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $employess = $this->employessRepository->find($id);

        if (empty($employess)) {
            Flash::error('Employess not found');

            return redirect(route('employesses.index'));
        }

        return view('employesses.edit')->with('employess', $employess);
    }

    /**
     * Update the specified Employess in storage.
     *
     * @param int $id
     * @param UpdateEmployessRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEmployessRequest $request)
    {
        $employess = $this->employessRepository->find($id);

        if (empty($employess)) {
            Flash::error('Employess not found');

            return redirect(route('employesses.index'));
        }

        $employess = $this->employessRepository->update($request->all(), $id);

        Flash::success('Employess updated successfully.');

        return redirect(route('employesses.index'));
    }

    /**
     * Remove the specified Employess from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $employess = $this->employessRepository->find($id);

        if (empty($employess)) {
            Flash::error('Employess not found');

            return redirect(route('employesses.index'));
        }

        $this->employessRepository->delete($id);

        Flash::success('Employess deleted successfully.');

        return redirect(route('employesses.index'));
    }
}
