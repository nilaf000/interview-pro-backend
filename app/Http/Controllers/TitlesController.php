<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTitlesRequest;
use App\Http\Requests\UpdateTitlesRequest;
use App\Repositories\TitlesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class TitlesController extends AppBaseController
{
    /** @var  TitlesRepository */
    private $titlesRepository;

    public function __construct(TitlesRepository $titlesRepo)
    {
        $this->titlesRepository = $titlesRepo;
    }

    /**
     * Display a listing of the Titles.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $titles = $this->titlesRepository->all();

        return view('titles.index')
            ->with('titles', $titles);
    }

    /**
     * Show the form for creating a new Titles.
     *
     * @return Response
     */
    public function create()
    {
        return view('titles.create');
    }

    /**
     * Store a newly created Titles in storage.
     *
     * @param CreateTitlesRequest $request
     *
     * @return Response
     */
    public function store(CreateTitlesRequest $request)
    {
        $input = $request->all();

        $titles = $this->titlesRepository->create($input);

        Flash::success('Titles saved successfully.');

        return redirect(route('titles.index'));
    }

    /**
     * Display the specified Titles.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $titles = $this->titlesRepository->find($id);

        if (empty($titles)) {
            Flash::error('Titles not found');

            return redirect(route('titles.index'));
        }

        return view('titles.show')->with('titles', $titles);
    }

    /**
     * Show the form for editing the specified Titles.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $titles = $this->titlesRepository->find($id);

        if (empty($titles)) {
            Flash::error('Titles not found');

            return redirect(route('titles.index'));
        }

        return view('titles.edit')->with('titles', $titles);
    }

    /**
     * Update the specified Titles in storage.
     *
     * @param int $id
     * @param UpdateTitlesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTitlesRequest $request)
    {
        $titles = $this->titlesRepository->find($id);

        if (empty($titles)) {
            Flash::error('Titles not found');

            return redirect(route('titles.index'));
        }

        $titles = $this->titlesRepository->update($request->all(), $id);

        Flash::success('Titles updated successfully.');

        return redirect(route('titles.index'));
    }

    /**
     * Remove the specified Titles from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $titles = $this->titlesRepository->find($id);

        if (empty($titles)) {
            Flash::error('Titles not found');

            return redirect(route('titles.index'));
        }

        $this->titlesRepository->delete($id);

        Flash::success('Titles deleted successfully.');

        return redirect(route('titles.index'));
    }
}
