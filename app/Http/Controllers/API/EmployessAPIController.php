<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateEmployessAPIRequest;
use App\Http\Requests\API\UpdateEmployessAPIRequest;
use App\Models\Employess;
use App\Repositories\EmployessRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use \DateTime;
use Carbon\Carbon;
use DB;
use Exception;
use App\Repositories\SalariesRepository;
use App\Repositories\TitlesRepository;
use App\Criteria\SearchNameCriteria;
/**
 * Class EmployessController
 * @package App\Http\Controllers\API
 */

class EmployessAPIController extends AppBaseController
{
    /** @var  EmployessRepository */
    private $employessRepository;
    private $salariesRepository;
    private $titlesRepository;

    public function __construct(EmployessRepository $employessRepo,SalariesRepository $salariesRepo,TitlesRepository $titlesRepo)
    {
        $this->employessRepository = $employessRepo;
        $this->salariesRepository = $salariesRepo;
        $this->titlesRepository = $titlesRepo;
    }

    /**
     * Display a listing of the Employess.
     * GET|HEAD /employesses
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $limit = $request->get('limit');
        $this->employessRepository->pushCriteria(new RequestCriteria($request));
        $this->employessRepository->pushCriteria(new LimitOffsetCriteria($request));

    

        $search = $request->get('searcha');
        if (isset($search)) {
            $this->employessRepository->pushCriteria(new SearchNameCriteria($search));
        }

        $employesses = $this->employessRepository->with(['salary','title'])->orderBy('id', 'desc')->paginate($limit);



        return $this->sendResponse($employesses->toArray(), 'Employees retrieved successfully');






    }

    /**
     * Store a newly created Employess in storage.
     * POST /employesses
     *
     * @param CreateEmployessAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateEmployessAPIRequest $request)
    {

       // DB::beginTransaction();
        try {
            $input = $request->all();

            $date_of_birth_obj = new DateTime($input['birth_date']);
            $date_of_birth = $date_of_birth_obj->format('Y-m-d');
    
    
            $hire_date_obj = new DateTime($input['hire_date']);
            $hire_date = $hire_date_obj->format('Y-m-d');
    
    
            $from_date_obj = new DateTime($input['from_date']);
            $from_date = $from_date_obj->format('Y-m-d');
    
            $to_date_obj = new DateTime($input['to_date']);
            $to_date = $to_date_obj->format('Y-m-d');
    
            $from_date_title_obj = new DateTime($input['from_date_title']);
            $from_date_title = $from_date_title_obj->format('Y-m-d');
    
            $to_date_title_obj = new DateTime($input['to_date_title']);
            $to_date_title = $to_date_title_obj->format('Y-m-d');
    
        
    
            $employee_data['birth_date'] = $date_of_birth;
            $employee_data['first_name'] = $input['first_name'];
            $employee_data['last_name'] = $input['last_name'];
            $employee_data['gender'] = $input['gender'];
            $employee_data['hire_date'] = $hire_date;
    
            $employess = $this->employessRepository->create($employee_data);

            $salary_data['employee_id'] = $employess['id'];
            $salary_data['salary'] = $input['salary'];
            $salary_data['from_date'] = $from_date;
            $salary_data['to_date'] = $to_date;


            $salaries = $this->salariesRepository->create($salary_data);

            $title_data['employee_id'] = $employess['id'];
            $title_data['title'] = $input['title'];
            $title_data['from_date'] = $from_date_title;
            $title_data['to_date'] = $to_date_title;


            $salaries = $this->titlesRepository->create($title_data);


    
            return $this->sendResponse($employess->toArray(), 'Employess saved successfully');

           // DB::commit();
    } catch (Exception $exception) {
       // DB::rollBack();
        return $this->sendError($exception, '401');

    }


  
    }

    /**
     * Display the specified Employess.
     * GET|HEAD /employesses/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Employess $employess */
        $employess = $this->employessRepository->find($id);

        if (empty($employess)) {
            return $this->sendError('Employess not found');
        }

        return $this->sendResponse($employess->toArray(), 'Employess retrieved successfully');
    }

    /**
     * Update the specified Employess in storage.
     * PUT/PATCH /employesses/{id}
     *
     * @param int $id
     * @param UpdateEmployessAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEmployessAPIRequest $request)
    {
        $input = $request->all();

       


        /** @var Employess $employess */
        $employess = $this->employessRepository->find($id);

        if (empty($employess)) {
            return $this->sendError('Employess not found');
        }

        $employess = $this->employessRepository->update($input, $id);




        $salaries = $this->salariesRepository->find($input['salary_id']);

      

        $salaries = $this->salariesRepository->update($input, $input['salary_id']);


        $titles = $this->titlesRepository->find($input['title_id']);

        if (empty($titles)) {
            return $this->sendError('Titles not found');
        }

        $titles = $this->titlesRepository->update($input, $input['title_id']);





        return $this->sendResponse($employess->toArray(), 'Employess updated successfully');
    }

    /**
     * Remove the specified Employess from storage.
     * DELETE /employesses/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Employess $employess */
        $employess = $this->employessRepository->find($id);

        if (empty($employess)) {
            return $this->sendError('Employess not found');
        }

        $employess->delete();

        return $this->sendSuccess('Employess deleted successfully');
    }
}
