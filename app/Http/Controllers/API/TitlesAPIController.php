<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTitlesAPIRequest;
use App\Http\Requests\API\UpdateTitlesAPIRequest;
use App\Models\Titles;
use App\Repositories\TitlesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class TitlesController
 * @package App\Http\Controllers\API
 */

class TitlesAPIController extends AppBaseController
{
    /** @var  TitlesRepository */
    private $titlesRepository;

    public function __construct(TitlesRepository $titlesRepo)
    {
        $this->titlesRepository = $titlesRepo;
    }

    /**
     * Display a listing of the Titles.
     * GET|HEAD /titles
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $titles = $this->titlesRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($titles->toArray(), 'Titles retrieved successfully');
    }

    /**
     * Store a newly created Titles in storage.
     * POST /titles
     *
     * @param CreateTitlesAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateTitlesAPIRequest $request)
    {
        $input = $request->all();

        $titles = $this->titlesRepository->create($input);

        return $this->sendResponse($titles->toArray(), 'Titles saved successfully');
    }

    /**
     * Display the specified Titles.
     * GET|HEAD /titles/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Titles $titles */
        $titles = $this->titlesRepository->find($id);

        if (empty($titles)) {
            return $this->sendError('Titles not found');
        }

        return $this->sendResponse($titles->toArray(), 'Titles retrieved successfully');
    }

    /**
     * Update the specified Titles in storage.
     * PUT/PATCH /titles/{id}
     *
     * @param int $id
     * @param UpdateTitlesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTitlesAPIRequest $request)
    {
        $input = $request->all();

        /** @var Titles $titles */
        $titles = $this->titlesRepository->find($id);

        if (empty($titles)) {
            return $this->sendError('Titles not found');
        }

        $titles = $this->titlesRepository->update($input, $id);

        return $this->sendResponse($titles->toArray(), 'Titles updated successfully');
    }

    /**
     * Remove the specified Titles from storage.
     * DELETE /titles/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Titles $titles */
        $titles = $this->titlesRepository->find($id);

        if (empty($titles)) {
            return $this->sendError('Titles not found');
        }

        $titles->delete();

        return $this->sendSuccess('Titles deleted successfully');
    }
}
