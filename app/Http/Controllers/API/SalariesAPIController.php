<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSalariesAPIRequest;
use App\Http\Requests\API\UpdateSalariesAPIRequest;
use App\Models\Salaries;
use App\Repositories\SalariesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class SalariesController
 * @package App\Http\Controllers\API
 */

class SalariesAPIController extends AppBaseController
{
    /** @var  SalariesRepository */
    private $salariesRepository;

    public function __construct(SalariesRepository $salariesRepo)
    {
        $this->salariesRepository = $salariesRepo;
    }

    /**
     * Display a listing of the Salaries.
     * GET|HEAD /salaries
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $salaries = $this->salariesRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($salaries->toArray(), 'Salaries retrieved successfully');
    }

    /**
     * Store a newly created Salaries in storage.
     * POST /salaries
     *
     * @param CreateSalariesAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateSalariesAPIRequest $request)
    {
        $input = $request->all();

        $salaries = $this->salariesRepository->create($input);

        return $this->sendResponse($salaries->toArray(), 'Salaries saved successfully');
    }

    /**
     * Display the specified Salaries.
     * GET|HEAD /salaries/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Salaries $salaries */
        $salaries = $this->salariesRepository->find($id);

        if (empty($salaries)) {
            return $this->sendError('Salaries not found');
        }

        return $this->sendResponse($salaries->toArray(), 'Salaries retrieved successfully');
    }

    /**
     * Update the specified Salaries in storage.
     * PUT/PATCH /salaries/{id}
     *
     * @param int $id
     * @param UpdateSalariesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSalariesAPIRequest $request)
    {
        $input = $request->all();

        /** @var Salaries $salaries */
        $salaries = $this->salariesRepository->find($id);

        if (empty($salaries)) {
            return $this->sendError('Salaries not found');
        }

        $salaries = $this->salariesRepository->update($input, $id);

        return $this->sendResponse($salaries->toArray(), 'Salaries updated successfully');
    }

    /**
     * Remove the specified Salaries from storage.
     * DELETE /salaries/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Salaries $salaries */
        $salaries = $this->salariesRepository->find($id);

        if (empty($salaries)) {
            return $this->sendError('Salaries not found');
        }

        $salaries->delete();

        return $this->sendSuccess('Salaries deleted successfully');
    }
}
