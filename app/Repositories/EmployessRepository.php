<?php

namespace App\Repositories;

use App\Models\Employess;
use App\Repositories\BaseRepository;

/**
 * Class EmployessRepository
 * @package App\Repositories
 * @version December 12, 2020, 5:04 am UTC
*/

class EmployessRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'emp_no',
        'birth_date',
        'first_name',
        'last_name',
        'gender',
        'hire_date'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Employess::class;
    }
}
