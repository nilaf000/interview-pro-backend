<?php

namespace App\Repositories;

use App\Models\Salaries;
use App\Repositories\BaseRepository;

/**
 * Class SalariesRepository
 * @package App\Repositories
 * @version December 12, 2020, 5:18 am UTC
*/

class SalariesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'employee_id',
        'emp_no',
        'salary',
        'from_date',
        'to_date'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Salaries::class;
    }
}
