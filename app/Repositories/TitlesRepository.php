<?php

namespace App\Repositories;

use App\Models\Titles;
use App\Repositories\BaseRepository;

/**
 * Class TitlesRepository
 * @package App\Repositories
 * @version December 12, 2020, 5:14 am UTC
*/

class TitlesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'employee_id',
        'emp_no',
        'title',
        'from_date',
        'to_date'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Titles::class;
    }
}
