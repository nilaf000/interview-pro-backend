<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;



/**
 * Class Titles
 * @package App\Models
 * @version December 12, 2020, 5:14 am UTC
 *
 * @property \App\Models\Employess $employee
 * @property integer $employee_id
 * @property integer $emp_no
 * @property string $title
 * @property string $from_date
 * @property string $to_date
 */
class Titles extends Model
{


    public $table = 'titles';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';




    public $fillable = [
        'employee_id',
        'title',
        'from_date',
        'to_date'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'employee_id' => 'integer',
        'title' => 'string',
        'from_date' => 'date',
        'to_date' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'employee_id' => 'nullable',
        'title' => 'required|string|max:50',
        'from_date' => 'required',
        'to_date' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function employee()
    {
        return $this->belongsTo(\App\Models\Employess::class, 'employee_id');
    }
}
