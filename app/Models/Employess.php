<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;



/**
 * Class Employess
 * @package App\Models
 * @version December 12, 2020, 5:04 am UTC
 *
 * @property integer $emp_no
 * @property string $birth_date
 * @property string $first_name
 * @property string $last_name
 * @property boolean $gender
 * @property string $hire_date
 */
class Employess extends Model
{


    public $table = 'employess';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';




    public $fillable = [
        'birth_date',
        'first_name',
        'last_name',
        'gender',
        'hire_date'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'birth_date' => 'date',
        'first_name' => 'string',
        'last_name' => 'string',
        'gender' => 'integer',
        'hire_date' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        // 'birth_date' => 'required',
        // 'first_name' => 'required|string|max:14',
        // 'last_name' => 'required|string|max:16',
        // 'gender' => 'required|boolean',
        // 'hire_date' => 'required'
    ];



    public function salary()
    {
        return $this->hasOne(\App\Models\Salaries::class,'employee_id');
    }


    public function title()
    {
        return $this->hasOne(\App\Models\Titles::class,'employee_id');
    }
    
}
