<!-- Employee Id Field -->
<div class="form-group">
    {!! Form::label('employee_id', 'Employee Id:') !!}
    <p>{{ $titles->employee_id }}</p>
</div>

<!-- Emp No Field -->
<div class="form-group">
    {!! Form::label('emp_no', 'Emp No:') !!}
    <p>{{ $titles->emp_no }}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $titles->title }}</p>
</div>

<!-- From Date Field -->
<div class="form-group">
    {!! Form::label('from_date', 'From Date:') !!}
    <p>{{ $titles->from_date }}</p>
</div>

<!-- To Date Field -->
<div class="form-group">
    {!! Form::label('to_date', 'To Date:') !!}
    <p>{{ $titles->to_date }}</p>
</div>

