<div class="table-responsive">
    <table class="table" id="titles-table">
        <thead>
            <tr>
                <th>Employee Id</th>
        <th>Emp No</th>
        <th>Title</th>
        <th>From Date</th>
        <th>To Date</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($titles as $titles)
            <tr>
                <td>{{ $titles->employee_id }}</td>
            <td>{{ $titles->emp_no }}</td>
            <td>{{ $titles->title }}</td>
            <td>{{ $titles->from_date }}</td>
            <td>{{ $titles->to_date }}</td>
                <td>
                    {!! Form::open(['route' => ['titles.destroy', $titles->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('titles.show', [$titles->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('titles.edit', [$titles->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
