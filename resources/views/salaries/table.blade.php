<div class="table-responsive">
    <table class="table" id="salaries-table">
        <thead>
            <tr>
                <th>Employee Id</th>
        <th>Emp No</th>
        <th>Salary</th>
        <th>From Date</th>
        <th>To Date</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($salaries as $salaries)
            <tr>
                <td>{{ $salaries->employee_id }}</td>
            <td>{{ $salaries->emp_no }}</td>
            <td>{{ $salaries->salary }}</td>
            <td>{{ $salaries->from_date }}</td>
            <td>{{ $salaries->to_date }}</td>
                <td>
                    {!! Form::open(['route' => ['salaries.destroy', $salaries->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('salaries.show', [$salaries->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('salaries.edit', [$salaries->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
