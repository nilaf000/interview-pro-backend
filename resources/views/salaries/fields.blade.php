<!-- Employee Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('employee_id', 'Employee Id:') !!}
    {!! Form::number('employee_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Emp No Field -->
<div class="form-group col-sm-6">
    {!! Form::label('emp_no', 'Emp No:') !!}
    {!! Form::number('emp_no', null, ['class' => 'form-control']) !!}
</div>

<!-- Salary Field -->
<div class="form-group col-sm-6">
    {!! Form::label('salary', 'Salary:') !!}
    {!! Form::number('salary', null, ['class' => 'form-control']) !!}
</div>

<!-- From Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('from_date', 'From Date:') !!}
    {!! Form::text('from_date', null, ['class' => 'form-control','id'=>'from_date']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#from_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- To Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('to_date', 'To Date:') !!}
    {!! Form::text('to_date', null, ['class' => 'form-control','id'=>'to_date']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#to_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('salaries.index') }}" class="btn btn-default">Cancel</a>
</div>
