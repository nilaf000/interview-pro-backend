<!-- Employee Id Field -->
<div class="form-group">
    {!! Form::label('employee_id', 'Employee Id:') !!}
    <p>{{ $salaries->employee_id }}</p>
</div>

<!-- Emp No Field -->
<div class="form-group">
    {!! Form::label('emp_no', 'Emp No:') !!}
    <p>{{ $salaries->emp_no }}</p>
</div>

<!-- Salary Field -->
<div class="form-group">
    {!! Form::label('salary', 'Salary:') !!}
    <p>{{ $salaries->salary }}</p>
</div>

<!-- From Date Field -->
<div class="form-group">
    {!! Form::label('from_date', 'From Date:') !!}
    <p>{{ $salaries->from_date }}</p>
</div>

<!-- To Date Field -->
<div class="form-group">
    {!! Form::label('to_date', 'To Date:') !!}
    <p>{{ $salaries->to_date }}</p>
</div>

