<div class="table-responsive">
    <table class="table" id="employesses-table">
        <thead>
            <tr>
                <th>Emp No</th>
        <th>Birth Date</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Gender</th>
        <th>Hire Date</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($employesses as $employess)
            <tr>
                <td>{{ $employess->emp_no }}</td>
            <td>{{ $employess->birth_date }}</td>
            <td>{{ $employess->first_name }}</td>
            <td>{{ $employess->last_name }}</td>
            <td>{{ $employess->gender }}</td>
            <td>{{ $employess->hire_date }}</td>
                <td>
                    {!! Form::open(['route' => ['employesses.destroy', $employess->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('employesses.show', [$employess->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('employesses.edit', [$employess->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
