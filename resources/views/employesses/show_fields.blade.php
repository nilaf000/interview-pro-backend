<!-- Emp No Field -->
<div class="form-group">
    {!! Form::label('emp_no', 'Emp No:') !!}
    <p>{{ $employess->emp_no }}</p>
</div>

<!-- Birth Date Field -->
<div class="form-group">
    {!! Form::label('birth_date', 'Birth Date:') !!}
    <p>{{ $employess->birth_date }}</p>
</div>

<!-- First Name Field -->
<div class="form-group">
    {!! Form::label('first_name', 'First Name:') !!}
    <p>{{ $employess->first_name }}</p>
</div>

<!-- Last Name Field -->
<div class="form-group">
    {!! Form::label('last_name', 'Last Name:') !!}
    <p>{{ $employess->last_name }}</p>
</div>

<!-- Gender Field -->
<div class="form-group">
    {!! Form::label('gender', 'Gender:') !!}
    <p>{{ $employess->gender }}</p>
</div>

<!-- Hire Date Field -->
<div class="form-group">
    {!! Form::label('hire_date', 'Hire Date:') !!}
    <p>{{ $employess->hire_date }}</p>
</div>

