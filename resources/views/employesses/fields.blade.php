<!-- Emp No Field -->
<div class="form-group col-sm-6">
    {!! Form::label('emp_no', 'Emp No:') !!}
    {!! Form::number('emp_no', null, ['class' => 'form-control']) !!}
</div>

<!-- Birth Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('birth_date', 'Birth Date:') !!}
    {!! Form::text('birth_date', null, ['class' => 'form-control','id'=>'birth_date']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#birth_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- First Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('first_name', 'First Name:') !!}
    {!! Form::text('first_name', null, ['class' => 'form-control','maxlength' => 14,'maxlength' => 14,'maxlength' => 14]) !!}
</div>

<!-- Last Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('last_name', 'Last Name:') !!}
    {!! Form::text('last_name', null, ['class' => 'form-control','maxlength' => 16,'maxlength' => 16,'maxlength' => 16]) !!}
</div>

<!-- Gender Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gender', 'Gender:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('gender', 0) !!}
        {!! Form::checkbox('gender', '1', null) !!}
    </label>
</div>


<!-- Hire Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hire_date', 'Hire Date:') !!}
    {!! Form::text('hire_date', null, ['class' => 'form-control','id'=>'hire_date']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#hire_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('employesses.index') }}" class="btn btn-default">Cancel</a>
</div>
