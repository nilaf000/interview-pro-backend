@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Employess
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($employess, ['route' => ['employesses.update', $employess->id], 'method' => 'patch']) !!}

                        @include('employesses.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection